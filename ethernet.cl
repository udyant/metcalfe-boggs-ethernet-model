;;; -*- mode: common-lisp -*-
;; This software draws upon material published in
;;        Communications of the ACM, July 1976
;;        Copyright © 1976, Association for Computing Machinery, Inc.
;; The material has been used under the permissions granted by said
;; copyright.
;;
;; This file contains an implementation of the Ethernet efficiency model
;; described in
;;        Ethernet: Distributed Packet Switching for Local Computer
;;                            Networks
;;                Robert M. Metcalfe and David R. Boggs
;;                   Xerox Palo Alto Research Center
;;
;; Let C := peak capacity in bits per second; 3 Mbps for the
;;          experimental Ethernet, 10 Mbps for my local cat5 cables,
;;          3 Mbps for my local WiFi LAN.
;;     T := number of seconds to detect a collision after beginning
;;          transmission; time of a slot, 16 µs for the experimental
;;          Ethernet.
;;     Q := stations queued to transmit a packet; Q >= 1.
;;          The probability of tranmission of a station in the current
;;          slot is (/ Q);  the probability of delay is (- 1 (/ Q)).
;;     P := number of bits in Ethernet packet.
;;     A := acquisition probability; the probability that exactly one
;;          station attempts a tranmission in a slot and so acquires the
;;          ether.
;;                          (expt (- 1 (/ Q)) (1- Q))
;;     W := waiting time; the mean number of slots of waiting in a
;;          contentious interval before a successful acquisition.
;;          Probability of no wait time is just A; probability of
;;          waiting 1 slot is (* A (- 1 A)); probability of waiting
;;          i slots is (* A (expt (- 1 A) i)).  The mean of this geometric
;;          distribution is (/ (- 1 A) A).
;;     E := efficiency; fraction of time Ethernet carries good packets.
;;          Packet tranmission takes (/ P C) seconds, while the mean
;;          acquisition time is (* W T).  Therefore, efficiency is
;;                          (/ (/ P C) (+ (/ P C) (* W T))).

(declaim (optimize safety))

(defpackage "ETHERNET"
  (:use "CL")
  (:export "ACQUISITION-PROBABILITY"
           "WAITING-TIME"
           "EFFICIENCY"
           "PACKET-LENGTH-EFFICIENCIES"
           "TABULATE-EFFICIENCIES"))
(in-package "ETHERNET")

(defparameter *scaled-unit-of-capacity* 1e3
  "Unit of bandwidth relative to 1 bit per second.")

(defparameter *base-channel-capacity* 288
  "Base measure of channel capacity.")

(defparameter *channel-capacity* (* *base-channel-capacity* *scaled-unit-of-capacity*)
  "Peak channel capacity in bits per second.")

(defparameter *scaled-unit-of-time* 1e-9
  "Unit of time relative to 1 second.")

(defparameter *base-time-slot* 4
  "Base measure of time slot.")

(defparameter *string-unit-of-time* nil
  "Short name of unit of time.")

(defparameter *time-of-slot* (* *base-time-slot* *scaled-unit-of-time*)
  "Slot time in seconds.")

(defparameter *packet-lengths* (list 8192 4096 2048 1024 512 48)
  "Various lengths for an Ethernet packet in bits.")

(defparameter *queued-stations* (list 1 2 3 4 5 10 32 64 128 256 512 768 1024 2048 4096 8192)
  "Various numbers of queued stations.")


(defun store-name-unit-of-time ()
  "Store the name of the time unit in *STRING-UNIT-OF-TIME* according to *SCALED-UNIT-OF-TIME*.

Argument:    none
Returns:     string"
  (let ((unit-name-alist '((1e-3 . "ms")
			   (1e-6 . "μs")
			   (1e-9 . "ns"))))
    (setf *string-unit-of-time* (rest (assoc *scaled-unit-of-time* unit-name-alist)))))


(defun acquisition-probability (queued-stations)
  "Probability of one station attempting a transmission in a slot and
acquiring the Ether.

Argument:    positive-integer
Returns:     number"
  (expt (- 1 (/ queued-stations)) (1- queued-stations)))


(defun waiting-time (acquisition-probability)
  "Number of slots waiting in a contentious interval before a
successful acquisition.

Argument:    number (in [0,1])
Returns:     number"
  (/ (- 1 acquisition-probability) acquisition-probability))


(defun efficiency (channel-capacity packet-length
                   waiting-time time-of-slot)
  "Fraction of time Ethernet carries good packets.

Arguments:   positive-integer positive-integer number positive-integer
Returns:     number"
  (let ((channel-fraction (/ packet-length channel-capacity)))
    (/ channel-fraction
       (+ channel-fraction (* waiting-time time-of-slot)))))


(defun packet-length-efficiencies (queued-stations)
  "For a given number of QUEUED-STATIONS, collect the efficiencies for
various packet lengths.

Argument:    positive-integer
Returns:     list of numbers"
  (loop for p in *packet-lengths*
      collect (efficiency *channel-capacity* p
                          (waiting-time (acquisition-probability
                                         queued-stations))
                          *time-of-slot*))
  ;; (mapcar #'(lambda (packet-length)
  ;; 	      (efficiency *channel-capacity* packet-length
  ;;                         (waiting-time (acquisition-probability
  ;;                                        queued-stations))
  ;;                         *time-of-slot*))
  ;; 	  *packet-lengths*)
  )

(defun tabulate-efficiencies ()
  "For various number of queued stations, print efficiencies.

Argument:    none
Returns:     nil"
  (store-name-unit-of-time)

  (write-line "Packet lengths (P) in bits.")
  (format t "~f Mbps capacity and ~f ~a slot.~%"
          (/ *channel-capacity* *scaled-unit-of-capacity*)
          (/ *time-of-slot* *scaled-unit-of-time*)
	  *string-unit-of-time*)
  (format t "    Q\\P ~{  P    = ~8d ~}~%" *packet-lengths*)
  (dolist (q *queued-stations*)
    (format t "~5d ~{  ~14,8f  ~}    ~%" q (packet-length-efficiencies q))))
