(defsystem :ethernet
  :default-component-class cl-source-file.cl
  :description "Metcalfe and Boggs Ethernet efficiency model."
  :version "0.0"
  :author "Udyant Wig <udyant.wig@gmail.com>"
  :licence "Undecided"
  :components ((:file "ethernet")))
